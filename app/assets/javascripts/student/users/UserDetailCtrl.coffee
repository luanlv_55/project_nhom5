
class UserDetailCtrl

    constructor: (@$log, @$location,@$rootScope, @$routeParams, @UserService, @security, @fileUpload, @$route) ->
        @$log.debug "constructing UserDetailController"
        @user = {}
        @initmssv = {}
        @initemail = {}
        @updateOk = false
        @updateError = false
        @updateAvataOk = false
        @updateAvataError = false
        @userLogin={}
        @loading = true
        @loading2 = true
        @getUser(@$rootScope.email)
        @file = {}
        if @user._id isnt undefined
            console.log @user
        @formOk = false
        @formDisable = ""
        @xdisable = ""

    uploadFile: () ->
        console.log("=>>>> uploadFile starting")
        @file = @myFile
        @loading2 = true
        @fileUpload.uploadFileToUrl(@file, @user._id.$oid)
        .then(
            (data) =>
                @loading2 = false
                @UserService.deleteAvatar(@user.avatarID)
                .then(
                    (data) =>
                        console.log "delete old avatar OK!"
                ,
                    (error) =>
                        console.log " delete old avatar error!"
                )
                @user.avatarID = data.data
                @UserService.updateUser(@user)
                .then(
                    (data) =>
                        @updateAvataOk = true
                ,
                    (error) =>
                        @updateAvataError = true
                )
        ,
            (error) =>
                @$log.error error
        )

    getUser: (email) ->
        @doUpdate = true if email
        if @doUpdate
            @$log.debug "getUser(#{email})"

            @UserService.getUser(email)
            .then(
                (data) =>
                    @$log.debug "Promise returned #{angular.toJson(data, true)} User"
                    @user = data
                    @initmssv = @user.mssv
                    @initemail = @user.email
                    @userLogin.email = @user.email
                    @loading = false
                    @loading2 = false
            ,
                (error) =>
                    @$log.error "Unable to get User: #{error}"
                    @loading = false
                    @loading2 = false
            )

    updateUser: () ->
        @updateError = false
        @loading = true
        @UserService.updateUser(@user)
        .then(
            (data) =>
                @$log.debug "Promise returned #{data} User"
                @updateOk = true
                @loading = false
        ,
            (error) =>
                @$log.error "Unable to update User: #{error}"
                @updateError = true
                @loading = false
        )
        if @user.email isnt @userLogin.email
            @$log.debug "update user ok!"
            @userLogin.newEmail =@user.email
            @$log.debug @userLogin
            @UserService.updateEmail(@userLogin)
            .then(
                (data) =>
                    @security.signout () ->
                        if($cookies isnt undefined)
                            delete $cookies["email"]
                            delete $cookies["XSRF-TOKEN"]
                            delete $cookies["role"]
                            window.location.href = 'http://localhost:9000/login'
                    #window.location.href = 'http://quanly2.herokuapp.com/login'
                    , () ->
                        console.log "signout Error!"
            ,
                (error) =>
                    @$log.error " error!"
            )

    resetError: () ->
        @userCreated = false
        @accountCreated = false
controllersModule.controller('UserDetailCtrl', UserDetailCtrl)