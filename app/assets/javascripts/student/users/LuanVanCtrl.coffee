
class LuanVanCtrl

    constructor: (@$log, @$location, @UserService, @$rootScope, @fileUpload) ->
        @lv = {}
        @user = {}
        @file = {}
        console.log @$rootScope.email
        @getLuanVan(@$rootScope.email)


    getLuanVan: (email) ->
        @$log.debug "getLuanVan()"

        @UserService.getUser(email)
        .then(
            (data) =>
                @$log.debug "Promise returned #{angular.toJson(data, true)} User"
                @user = data
                if @$rootScope.dkluanvan is true
                    @UserService.getLuanVan(@user._id.$oid)
                    .then(
                        (data) =>
                            @$log.debug "Promise returned Users count " + data
                            @lv = data
                            @UserService.getFileInfo(@lv.fileID)
                            .then(
                                (data) =>
                                    @file = data
                            ,
                                (error) =>
                                    console.log "error"
                            )
                    ,
                        (error) =>
                            @$log.error "Unable to get Users count: #{error}"

                    )

        ,
            (error) =>
                @$log.error "Unable to get User: #{error}"
                @loading = false
                @loading2 = false
        )

    dangKyLai: () ->
        @UserService.deleteAvatar(@lv.fileID)
        .then(
            (data) =>
                console.log "delete file OK"
        ,
            (error) =>
                console.log "delete file error!"

        )
        @UserService.deleteLV(@lv._id.$oid, @lv.sinhvienID)
        .then(
            (data) =>
                @$rootScope.dkluanvan = false
                $location.path('/teachers')
        ,
            (error) =>
                window.alert "Co loi"
        )
controllersModule.controller('LuanVanCtrl', LuanVanCtrl)