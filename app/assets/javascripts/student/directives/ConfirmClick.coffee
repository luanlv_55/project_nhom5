directivesModule
.directive 'confirmClick', () ->
  priority: 1,
  terminal: true,
  link: (scope, element, attrs) ->
    msg = attrs.confirmClick || "Are you sure?"
    clickAction = attrs.ngClick

    # handle click on button
    element.bind 'click', () ->
      # First Click
      if window.confirm(msg)
        scope.$eval(clickAction)

.directive 'ensureUnique', ['$http', 'serverAPI', (@$http, serverAPI) ->
  require: 'ngModel'
  link: (scope, element, attrs, ctrl) ->
    scope.$watch(attrs.ngModel, () ->
      serverAPI.ensureUnique ctrl.$modelValue,
        (data) ->
          ctrl.$setValidity 'unique', data.isUnique
      ,
        () ->
          ctrl.$setValidity 'unique', false)
]


.directive 'mssvUnique', ['$http', 'serverAPI', (@$http, serverAPI) ->
  require: 'ngModel'
  link: (scope, element, attrs, ctrl) ->
    scope.$watch(attrs.ngModel, () ->
      serverAPI.mssvUnique ctrl.$modelValue,
        (data) ->
          ctrl.$setValidity 'unique', data.isUnique
      ,
        () ->
          ctrl.$setValidity 'unique', false)
]

.directive 'emailUnique', ['$http', 'serverAPI', (@$http, serverAPI) ->
  require: 'ngModel'
  link: (scope, element, attrs, ctrl) ->
    scope.$watch(attrs.ngModel, () ->
      serverAPI.emailUnique ctrl.$modelValue,
        (data) ->
          ctrl.$setValidity 'unique', data.isUnique
      ,
        () ->
          ctrl.$setValidity 'unique', false)
]

.directive 'fileModel', ['$parse', ($parse) ->
  restrict: 'A',
  link: (scope, element, attrs) ->
    model = $parse(attrs.fileModel)
    modelSetter = model.assign
    element.bind 'change', () ->
      scope.$apply () ->
        modelSetter(scope, element[0].files[0])
]