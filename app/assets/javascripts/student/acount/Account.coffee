class SignupCtrl
    constructor: (@$rootScope,@$scope,@$location ,security)->
        console.log "!!! Constructor SignupCtrl"
        @$rootScope.signupError = false
        @signup = () ->
          security.signup @$scope.userform,
            () ->
              $location.path '/',
            () ->
              @$rootScope.signupError = true

class UpdatePasswordCtrl
    constructor: (@$rootScope,@$scope,@$location,security, $cookies)->
        @loading = false
        @error = false
        @$scope.userform = {}
        @$scope.userform.email = $rootScope.email
        @passwordMatch = false
        @repassword = ""
        @disabled = ""
        @updateOK = false
        @updatePassword = () ->
            @loading = true
            console.log "update password click"
            security.updatePassword @$scope.userform,
                () =>
                    @$scope.userform.password = ""
                    @repassword = ""
                    @updateOK = true
                    @loading = false
            ,
                () =>
                    @error = true
                    @loading = false

class MainCtrl
    constructor: (@$rootScope, @$scope, @$location, security, serverAPI, @$cookies) ->
        @email = @$rootScope.email
        @$scope.isActive = (viewLocation) ->
          viewLocation is $location.path()

        @$scope.account = () ->
          serverAPI.userinfo (data) ->
            @$scope.user = data
          , () ->
            @$scope.user = 'Error...'

        @$scope.deleteAccount = () ->
          serverAPI.deleteAccount () ->
            @$location.path '/admin#/'
          , () ->
            console.log "!! err"
        @$scope.signoutError = false

        @$scope.signout = () ->
            security.signout () ->
              if($cookies isnt undefined)
                delete $cookies["email"]
                delete $cookies["XSRF-TOKEN"]
                delete $cookies["role"]
                console.log "signout success!"
                #window.location.href = 'http://localhost:9000/login'
                window.location.href = 'http://quanly2.herokuapp.com/login'
            , () ->
                console.log "signout Error!"


class FileCtrl
    constructor: (@$scope, @$location, fileUpload) ->
      console.log("=>>>> FileCtrl Started")
      @$scope.uploadFile = () ->
        console.log("=>>>> uploadFile starting")
        file = $scope.myFile
        fileUpload.uploadFileToUrl(file)


controllersModule.controller('SignupCtrl', SignupCtrl)
controllersModule.controller('MainCtrl', MainCtrl)
controllersModule.controller('UpdatePasswordCtrl', UpdatePasswordCtrl)
controllersModule.controller('FileCtrl', FileCtrl)

