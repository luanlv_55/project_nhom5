
class DangKyCtrl

    constructor: (@$log, @$location, @$routeParams, @TeacherService, @UserService, @security, @$rootScope, @fileUpload ) ->
        @teacher = {}
        @user = {}
        @lv = {}
        @getTeacher(@$routeParams.uuid)
        @getUser(@$rootScope.email)
        @file = {}
        @myFile = {}

    submit: () ->
        console.log("=>>>> uploadFile starting")
        @file = @myFile
        console.log "====>" + @file
        @fileUpload.uploadFileToUrl2(@file, @user._id.$oid)
        .then(
            (data) =>
                @lv.fileID = data.data
                @UserService.createLV(@lv)
                .then(
                    (data) =>
                        @$log.debug "Promise returned #{data} User"
                        @$rootScope.dkluanvan = true
                        @$location.path '/luanvan'
                ,
                    (error) =>
                        @$log.error "Unable to create User: #{error}"
                )
        ,
            (error) =>
                @$log.error error
        )


    getUser: (email) ->
        @$log.debug "getUser(#{email})"

        @UserService.getUser(email)
        .then(
            (data) =>
                @$log.debug "Promise returned #{angular.toJson(data, true)} User"
                @user = data
                @lv.sinhvienID = @user._id.$oid
                @lv.sinhvien = @user.name
        ,
            (error) =>
                @$log.error "Unable to get User: #{error}"

        )

    getTeacher: (uuid) ->
        @TeacherService.getTeacher(uuid)
        .then(
            (data) =>
                @$log.debug "Promise returned #{angular.toJson(data, true)} Teacher"
                @teacher = data
                @lv.giaovienID = @teacher._id.$oid
                @lv.giaovien = @teacher.ten
        ,
            (error) =>
                @$log.error "Unable to get Teacher: #{error}"
        )

    getLuanVan: (uuid) ->
        @TeacherService.getLuanVan(uuid)
        .then(
            (data) =>
                @$log.debug "Promise returned #{angular.toJson(data, true)} luan van"
                @luanvan = data
        ,
            (error) =>
                @$log.error "Unable to get luan van #{error}"
        )


controllersModule.controller('DangKyCtrl', DangKyCtrl)