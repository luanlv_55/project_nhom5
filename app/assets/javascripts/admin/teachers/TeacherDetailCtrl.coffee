
class TeacherDetailCtrl

    constructor: (@$log, @$location, @$routeParams, @TeacherService, @security, @fileUpload) ->
        @$log.debug "constructing TeacherDetailController"
        @teacher = {}
        @initemail = {}
        @teacherCreated = false
        @accountCreated = false
        @updateOk = false
        @updateError = false
        @updateAvataOk = false
        @updateAvataError = false
        @teacherLogin ={}
        @doUpdate = false
        @loading = true
        @loading2 = true
        @getTeacher(@$routeParams.uuid)
        @file = {}
        @teacherAccount = {}
        if @teacher._id isnt undefined
            console.log @teacher
        @formOk = false
        @formDisable = ""

        @loading3 = true
        @loading4 = true

    uploadFile: () ->
        console.log("=>>>> uploadFile starting")
        @file = @myFile
        @loading2 = true
        @fileUpload.uploadFileToUrl(@file, @teacher._id.$oid)
        .then(
            (data) =>
                @loading2 = false
                @TeacherService.deleteAvatar(@teacher.avatarID)
                .then(
                    (data) =>
                        console.log "delete old avatar OK!"
                ,
                    (error) =>
                        console.log " delete old avatar error!"
                )
                @teacher.avatarID = data.data
                @TeacherService.updateTeacher(@teacher)
                .then(
                    (data) =>
                        @updateAvataOk = true
                ,
                    (error) =>
                        @updateAvataError = true
                )
        ,
            (error) =>
                @loading2 = false
                @$log.error error
        )

    getTeacher: (uuid) ->
        @doUpdate = true if uuid
        if @doUpdate
            @$log.debug "getTeacher(#{uuid})"

            @TeacherService.getTeacher(uuid)
            .then(
                (data) =>
                    @$log.debug "Promise returned #{angular.toJson(data, true)} Teacher"
                    @teacher = data
                    @initmssv = @teacher.mssv
                    @initemail = @teacher.email
                    @teacherLogin.email = @teacher.email
                    @loading = false
                    @loading2 = false
            ,
                (error) =>
                    @$log.error "Unable to get Teacher: #{error}"
                    @loading = false
                    @loading2 = false
            )
        else
            @loading = false

    updateTeacher: () ->
        @loading = true
        @TeacherService.updateTeacher(@teacher)
        .then(
            (data) =>
                @$log.debug "Promise returned #{data} Teacher"
                @updateOk = true
                @loading = false
        ,
            (error) =>
                @$log.error "Unable to update Teacher: #{error}"
                @updateError = true
                @loading = false
        )

        if @teacher.email isnt @teacherLogin.email
            @$log.debug "update teacher ok!"
            @teacherLogin.newEmail = @teacher.email
            @$log.debug @teacherLogin
            @TeacherService.updateEmail(@teacherLogin)
            .then(
                (data) =>
                    @teacherLogin.email = @teacherLogin.newEmail
            ,
                (error) =>
                    @$log.error " error!"
            )

    createTeacher: () ->
        @$log.debug "createTeacher()"
        @loading3 = true
        @loading4 = true
        @teacher.active = false
        @teacher.avatarID = null
        @TeacherService.createTeacher(@teacher)
        .then(
            (data) =>
                @$log.debug "Promise returned #{data} Teacher"
                @loading3 = false
                @teacherCreated = true
                @teacherAccount.email = @teacher.email
                @teacherAccount.password = '1'
                @teacherAccount.role = 'teacher'
                @TeacherService.createAccount @teacherAccount
                .then( (data) =>
                    @loading4 = false
                    @accountCreated = true
                ,
                    (error) =>
                        console.log "failed create teacher"
                )
                @teacher = {}

        ,
            (error) =>
                @$log.error "Unable to create Teacher: #{error}"
        )
        window.showOverlay()

    deleteTeacher: (email) ->
        @$log.debug "deleteTeacher(#{email})"

        @TeacherService.deleteTeacher(email)
        .then(
            (data) =>
                @$log.debug "Promise returned #{angular.toJson(data, true)} Teacher"
                @TeacherService.deleteTeacherAccount(email)
                .then(
                    (data) =>
                        @$log.debug "delete teacher account ok!"
                ,
                    (error) =>
                        @$log.debug "delete teacher account error"
                )
                @TeacherService.deleteToken(email)
                .then(
                    (data) =>
                        @$log.debug "delete Token ok!"
                ,
                    (error) =>
                        @$log.debug "delete Token error"
                )
        ,
            (error) =>
                @$log.error "Unable to delete Teacher: #{error}"
        )

    resetError: () ->
        @teacherCreated = false
        @accountCreated = false
controllersModule.controller('TeacherDetailCtrl', TeacherDetailCtrl)