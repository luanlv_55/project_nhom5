
class UserService
  @headers = {'Accept': 'application/json', 'Content-Type': 'application/json'}
  @defaultConfig = { headers: @headers }

  constructor: (@$log, @$http, @$q) ->
    @$log.debug "constructing UserService"

  listAllUsers: () ->
    @$log.debug "listUsers()"
    deferred = @$q.defer()

    @$http.get("/users")
    .success((data, status, headers) =>
      @$log.info("Successfully listed Users - status #{status}")
      deferred.resolve(data)
    )
    .error((data, status, headers) =>
      @$log.error("Failed to list Users - status #{status}")
      deferred.reject(data)
    )
    deferred.promise

  listUsers: (page, perPage, searchText, searchTextMssv) ->
    @$log.debug "listUsers()"
    deferred = @$q.defer()

    @$http.get("/users/#{page}/#{perPage}?searchText=#{searchText}&searchTextMssv=#{searchTextMssv}")
    .success((data, status, headers) =>
      @$log.info("Successfully listed Users - status #{status}")
      deferred.resolve(data)
    )
    .error((data, status, headers) =>
      @$log.error("Failed to list Users - status #{status}")
      deferred.reject(data)
    )
    deferred.promise

  getFiles: (uuid) ->
    @$log.debug "getFiles()"
    deferred = @$q.defer()

    @$http.get("/files/#{uuid}")
    .success((data, status, headers) =>
      @$log.info("Successfully listed Files - status #{status}")
      deferred.resolve(data)
    )
    .error((data, status, headers) =>
      @$log.error("Failed to list Files - status #{status}")
      deferred.reject(data)
    )
    deferred.promise

  uploadFile: (file) ->
    @$log.debug "createUser API"
    deferred = @$q.defer()

    @$http.post("/api/files", file)
    .success((data, status, headers) =>
      @$log.info("Successfully created File - status #{status}")
      deferred.resolve(data)
    )
    .error((data, status, headers) =>
      @$log.error("Failed to create user - status #{status}")
      deferred.reject(data)
    )
    deferred.promise

  countUsers: (searchText, searchTextMssv) ->
    @$log.debug "countUsers()"
    deferred = @$q.defer()

    @$http.get("/users/count?searchText=#{searchText}&searchTextMssv=#{searchTextMssv}")
    .success((data, status, headers) =>
      @$log.info("Successfully counted Users - status #{status}")
      deferred.resolve(data)
    )
    .error((data, status, headers) =>
      @$log.error("Failed to count Users - status #{status}")
      deferred.reject(data)
    )
    deferred.promise

  getUser: (uuid) ->
    @$log.debug "getUser()"
    deferred = @$q.defer()

    @$http.get("/user/#{uuid}")
    .success((data, status, headers) =>
      @$log.info("Successfully retrieve User - status #{status}")
      deferred.resolve(data)
    )
    .error((data, status, headers) =>
      @$log.error("Failed to retrieve User - status #{status}")
      deferred.reject(data)
    )
    deferred.promise


  deleteUser: (email) ->
    @$log.debug "deleteUser()"
    deferred = @$q.defer()

    @$http.delete("/user/#{email}")
    .success((data, status, headers) =>
      @$log.info("Successfully remove User - status #{status}")
      deferred.resolve(data)
    )
    .error((data, status, headers) =>
      @$log.error("Failed to remove User - status #{status}")
      deferred.reject(data)
    )
    deferred.promise

  getLuanVan: (id) ->
    @$log.debug "getLuanVan()"
    deferred = @$q.defer()

    @$http.get("/luanvan/get/#{id}")
    .success((data, status, headers) =>
      @$log.info("Successfully retrieve User - status #{status}")
      deferred.resolve(data)
    )
    .error((data, status, headers) =>
      @$log.error("Failed to retrieve User - status #{status}")
      deferred.reject(data)
    )
    deferred.promise

  getFileInfo: (id) ->
    @$log.debug "getUser()"
    deferred = @$q.defer()

    @$http.get("/api/fileinfo/#{id}")
    .success((data, status, headers) =>
      @$log.info("Successfully retrieve File info - status #{status}")
      deferred.resolve(data)
    )
    .error((data, status, headers) =>
      @$log.error("Failed to retrieve File info - status #{status}")
      deferred.reject(data)
    )
    deferred.promise

  deleteUserAccount: (emailAccount) ->
    @$log.debug "deleteUser()"
    deferred = @$q.defer()
    @$http.delete("/api/auth/deleteUserWithEmail/#{emailAccount}")
    .success((data, status, headers) =>
      @$log.info("Successfully remove User - status #{status}")
      deferred.resolve(data)
    )
    .error((data, status, headers) =>
      @$log.error("Failed to remove User - status #{status}")
      deferred.reject(data)
    )
    deferred.promise

  deleteAvatar: (uuid) ->
    @$log.debug "deleteAvatar()"
    deferred = @$q.defer()
    @$http.delete("/api/fileupload/delete/#{uuid}")
    .success((data, status, headers) =>
      @$log.info("Successfully delete avatar - status #{status}")
      deferred.resolve(data)
    )
    .error((data, status, headers) =>
      @$log.error("Failed to delete avatar - status #{status}")
      deferred.reject(data)
    )
    deferred.promise

  deleteToken: (emailAccount) ->
    @$log.debug "deleteUser()"
    deferred = @$q.defer()
    @$http.delete("/api/auth/deleteTokenWithEmail/#{emailAccount}")
    .success((data, status, headers) =>
      @$log.info("Successfully remove User - status #{status}")
      deferred.resolve(data)
    )
    .error((data, status, headers) =>
      @$log.error("Failed to remove User - status #{status}")
      deferred.reject(data)
    )
    deferred.promise

  createUser: (user) ->
    @$log.debug "createUser #{angular.toJson(user, true)}"
    deferred = @$q.defer()

    @$http.post('/user', user)
    .success((data, status, headers) =>
      @$log.info("Successfully created User - status #{status}")
      deferred.resolve(data)
    )
    .error((data, status, headers) =>
      @$log.error("Failed to create user - status #{status}")
      deferred.reject(data)
    )
    deferred.promise

  createAccount: (account) ->
    deferred = @$q.defer()
    @$http.post('/api/auth/user', account)
    .success((data, status, headers) =>
        @$log.info("Successfully created Account - status #{status}")
        deferred.resolve(data)
    )
    .error((data, status, headers) =>
        @$log.error("Failed to create Account - status #{status}")
        deferred.reject(data)
    )
    deferred.promise

  updateUser: (user) ->
    @$log.debug "updateUser #{angular.toJson(user, true)}"
    deferred = @$q.defer()

    @$http.post('/user/update', user)
    .success((data, status, headers) =>
      @$log.info("Successfully update User - status #{status}")
      deferred.resolve(data)
    )
    .error((data, status, headers) =>
      @$log.error("Failed to update user - status #{status}")
      deferred.reject(data)
    )
    deferred.promise

  updateEmail: (userLogin) ->
    deferred = @$q.defer()
    @$http.post('/api/auth/userEmail', userLogin)
    .success((data, status, headers) =>
      deferred.resolve(data)
    )
    .error((data, status, headers) =>
      deferred.reject(data)
    )
    deferred.promise

  generateUUID: () ->
    @$log.debug "generate UUID"
    deferred = @$q.defer()

    @$http.get("/randomUUID")
    .success((data, status, headers) =>
      @$log.info("Successfully retrieve UUID - status #{status}")
      deferred.resolve(data)
      @$log.debug "UUID #{data}"
    )
    .error((data, status, headers) =>
      @$log.error("Failed to retrieve UUID - status #{status}")
      deferred.reject(data)
    )
    deferred.promise



servicesModule.service('UserService', UserService)