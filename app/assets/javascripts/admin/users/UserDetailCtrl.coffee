
class UserDetailCtrl

    constructor: (@$log, @$location,@$rootScope, @$routeParams, @UserService, @security, @fileUpload) ->
        @$log.debug "constructing UserDetailController"
        @user = {}
        @initmssv = {}
        @initemail = {}
        @userCreated = false
        @accountCreated = false
        @updateOk = false
        @updateError = false
        @updateAvataOk = false
        @updateAvataError = false
        @doUpdate = false
        @loading = true
        @loading2 = true
        @getUser(@$routeParams.uuid)
        @userLogin={}
        @file = {}
        @userAccount = {}
        if @user._id isnt undefined
            console.log @user
        @formOk = false
        @formDisable = ""
        @loading3 = true
        @loading4 = true

    uploadFile: () ->
        console.log("=>>>> uploadFile starting")
        @file = @myFile
        @loading2 = true
        @fileUpload.uploadFileToUrl(@file, @user._id.$oid)
        .then(
            (data) =>
                @loading2 = false
                @UserService.deleteAvatar(@user.avatarID)
                .then(
                    (data) =>
                            console.log "delete old avatar OK!"
                ,
                    (error) =>
                            console.log " delete old avatar error!"
                )
                @user.avatarID = data.data
                @UserService.updateUser(@user)
                .then(
                    (data) =>
                        @updateAvataOk = true
                ,
                    (error) =>
                        @updateAvataError = true
                )
        ,
            (error) =>
                @loading2 = false
                @$log.error error
        )

    getUser: (uuid) ->

        @doUpdate = true if uuid
        if @doUpdate
            @$log.debug "getUser(#{uuid})"

            @UserService.getUser(uuid)
            .then(
                (data) =>
                    @$log.debug "Promise returned #{angular.toJson(data, true)} User"
                    @user = data
                    @initmssv = @user.mssv
                    @initemail = @user.email
                    @userLogin.email = @user.email
                    @loading = false
                    @loading2 = false
            ,
                (error) =>
                    @$log.error "Unable to get User: #{error}"
                    @loading = false
                    @loading2 = false
            )
        else
            @loading = false

    updateUser: () ->
        @updateError = false
        @loading = true
        @UserService.updateUser(@user)
        .then(
            (data) =>
                @$log.debug "Promise returned #{data} User"
                @updateOk = true
                @loading = false
        ,
            (error) =>
                @$log.error "Unable to update User: #{error}"
                @updateError = true
                @loading = false
        )
        if @user.email isnt @userLogin.email
            @$log.debug "update user ok!"
            @userLogin.newEmail =@user.email
            @$log.debug @userLogin
            @UserService.updateEmail(@userLogin)
            .then(
                (data) =>
                    @userLogin.email = @userLogin.newEmail
            ,
                (error) =>
                    @$log.error " error!"
            )


    createUser: () ->
        @$log.debug "createUser()"
        @loading3 = true
        @loading4 = true
        @user.active = false
        @user.luanvan = false
        @user.avatarID = null
        @UserService.createUser(@user)
        .then(
            (data) =>
                @loading3 = false
                @$log.debug "Promise returned #{data} User"
                @userCreated = true
                @userAccount.email = @user.email
                @userAccount.password = '1'
                @userAccount.role = 'student'
                @UserService.createAccount(@userAccount)
                .then(
                    (data) =>
                        @loading4 = false
                        @accountCreated = true
                ,
                    (error) =>
                        console.log "failed create user"
                )
                @user = {}

        ,
            (error) =>
                @$log.error "Unable to create User: #{error}"
        )
        window.showOverlay()

    deleteUser: (email) ->
        @$log.debug "deleteUser(#{email})"

        @UserService.deleteUser(email)
        .then(
            (data) =>
                @$log.debug "Promise returned #{angular.toJson(data, true)} User"
                @UserService.deleteUserAccount(email)
                .then(
                    (data) =>
                        @$log.debug "delete user account ok!"
                ,
                    (error) =>
                        @$log.debug "delete user account error"
                )
                @UserService.deleteToken(email)
                .then(
                    (data) =>
                        @$log.debug "delete Token ok!"
                ,
                    (error) =>
                        @$log.debug "delete Token error"
                )
        ,
            (error) =>
                @$log.error "Unable to delete User: #{error}"
        )


    resetError: () ->
        @userCreated = false
        @accountCreated = false
controllersModule.controller('UserDetailCtrl', UserDetailCtrl)