
class UserCtrl

    constructor: (@$log, @$location, @UserService) ->
        @$log.debug "constructing UserController"
        @loading = true
        @currentPage = 1
        @numPerPage = 10
        @maxSize = 5
        @bigTotalItems = 0
        @users = []
        @preSearchText = ""
        @searchText = ""
        @preSearchTextMssv = ""
        @searchTextMssv = ""
        @getAllUsers()

    clear: () ->
        @searchText = ""
        @preSearchText = ""
        @preSearchTextMssv = ""
        @searchTextMssv = ""
        @loading=true
        @getAllUsers()

    search: () ->
        @searchText = @preSearchText
        @searchTextMssv = @preSearchTextMssv
        @loading=true
        @currentPage = 1
        @getAllUsers()

    getAllUsers: () ->
        @$log.debug "getAllUsers()"
        @UserService.countUsers(@searchText, @searchTextMssv)
        .then(
            (data) =>
                @$log.debug "Promise returned Users count " + data
                @bigTotalItems = Math.ceil(data / @numPerPage)
        ,
            (error) =>
                @$log.error "Unable to get Users count: #{error}"
                @bigTotalItems = 0
        )

        @UserService.listUsers(@currentPage - 1, @numPerPage, @searchText, @searchTextMssv)
        .then(
            (data) =>
                @$log.debug "Promise returned #{data.length} Users"
                @users = data
                @loading = false
        ,
            (error) =>
                @$log.error "Unable to get Users: #{error}"
                @loading = false
        )



    pageNext: () ->
        @currentPage = @currentPage + 1
        @$log.debug "Page changed to: #{@currentPage}"
        @loading = true
        @getAllUsers()

    pagePrev: () ->
        @currentPage = @currentPage - 1
        @$log.debug "Page changed to: #{@currentPage}"
        @loading = true
        @getAllUsers()

    deleteUser: (email) ->
        @$log.debug "deleteUser(#{email})"

        @UserService.deleteUser(email)
        .then(
            (data) =>
                @$log.debug "Promise returned #{angular.toJson(data, true)} User"
                @loading = true
                @getAllUsers()
                @UserService.deleteUserAccount(email)
                .then(
                    (data) =>
                        @$log.debug "delete user account ok!"
                ,
                    (error) =>
                        @$log.debug "delete user account error"
                )
                @UserService.deleteToken(email)
                .then(
                    (data) =>
                        @$log.debug "delete Token ok!"
                ,
                    (error) =>
                        @$log.debug "delete Token error"
                )
        ,
            (error) =>
                @$log.error "Unable to delete User: #{error}"
        )

controllersModule.controller('UserCtrl', UserCtrl)