class LuanvanService
  @headers = {'Accept': 'application/json', 'Content-Type': 'application/json'}
  @defaultConfig = { headers: @headers }

  constructor: (@$log, @$http, @$q) ->
    @$log.debug "constructing Luanvanervice"

  listLuanvan: (page, perPage, searchText, trangthai, malv, sinhvien, giaovien) ->
    @$log.debug "listLuanvan()"
    deferred = @$q.defer()

    @$http.get("/luanvan/list/#{page}/#{perPage}?searchText=#{searchText}&trangthai=#{trangthai}&malv=#{malv}&sinhvien=#{sinhvien}&giaovien=#{giaovien}")
    .success((data, status, headers) =>
      @$log.info("Successfully listed Luanvan - status #{status}")
      deferred.resolve(data)
    )
    .error((data, status, headers) =>
      @$log.error("Failed to list Luanvan - status #{status}")
      deferred.reject(data)
    )
    deferred.promise


  countLuanvan: (searchText, trangthai, malv, sinhvien, giaovien) ->
    @$log.debug "countLuanvan()"
    deferred = @$q.defer()
    @$http.get("/luanvan/list/count?searchText=#{searchText}&trangthai=#{trangthai}&malv=#{malv}&sinhvien=#{sinhvien}&giaovien=#{giaovien}")
    .success((data, status, headers) =>
      @$log.info("Successfully counted Luanvan - status #{status}")
      deferred.resolve(data)
    )
    .error((data, status, headers) =>
      @$log.error("Failed to count Luanvan - status #{status}")
      deferred.reject(data)
    )
    deferred.promise


  deleteLuanvan: (uuid) ->
    @$log.debug "deleteLuanvan()"
    deferred = @$q.defer()

    @$http.delete("/luanvan/list/#{uuid}")
    .success((data, status, headers) =>
      @$log.info("Successfully remove Luanvan - status #{status}")
      deferred.resolve(data)
    )
    .error((data, status, headers) =>
      @$log.error("Failed to remove Luanvan - status #{status}")
      deferred.reject(data)
    )
    deferred.promise

  huybo: (luanvan) ->
    deferred = @$q.defer()
    @$http.post("/luanvan/set/huy", luanvan)
    .success((data, status, headers) =>
      @$log.info("Successfully created User - status #{status}")
      deferred.resolve(data)
    )
    .error((data, status, headers) =>
      @$log.error("Failed to create user - status #{status}")
      deferred.reject(data)
    )
    deferred.promise

  chapnhan: (luanvan) ->
    deferred = @$q.defer()
    @$http.post("/luanvan/set/ok", luanvan)
    .success((data, status, headers) =>
      @$log.info("Successfully created User - status #{status}")
      deferred.resolve(data)
    )
    .error((data, status, headers) =>
      @$log.error("Failed to create user - status #{status}")
      deferred.reject(data)
    )
    deferred.promise



servicesModule.service('LuanvanService', LuanvanService)