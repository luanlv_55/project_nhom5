
class LuanVanCtrl

    constructor: (@$log, @$location, @UserService, @$routeParams, @$rootScope, @LuanvanService) ->
        @lv = {}
        @user = {}
        @file = {}
        @getLuanVan(@$routeParams.id)



    getLuanVan: (id) ->
        @UserService.getLuanVan(id)
        .then(
            (data) =>
                @$log.debug "Promise returned Users count " + data
                @lv = data
                @UserService.getUser(@lv.sinhvienID)
                .then(
                    (data) =>
                        @user = data
                ,
                    (error) =>
                        console.log "error get User information"

                )
                .then(
                    (data) =>
                        @$log.debug "Promise returned #{angular.toJson(data, true)} User"
                        @user = data
                ,
                    (error) =>
                        @$log.error "Unable to get User: #{error}"
                )
                @UserService.getFileInfo(@lv.fileID)
                .then(
                    (data) =>
                        @file = data
                ,
                    (error) =>
                        console.log "error"
                )
        ,
            (error) =>
                @$log.error "Unable to get Users count: #{error}"
        )

    chapnhan: (malv) ->
        @tmp = {"ma": malv}
        @LuanvanService.chapnhan(@tmp)
        .then(
            (data) =>
                console.log "da chap nhan"
                @lv.chapnhan = true

        ,
            (error) =>
                console.log "co loi"
        )

    huybo: (malv) ->
        @tmp = {"ma": malv}
        @LuanvanService.huybo(@tmp)
        .then(
            (data) =>
                console.log "da huy bo"
                @lv.chapnhan = false
        ,
            (error) =>
                console.log "co loi"
        )
controllersModule.controller('LuanVanCtrl', LuanVanCtrl)