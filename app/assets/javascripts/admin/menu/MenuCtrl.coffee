class MenuCtrl
    constructor: (@$log, @$location, @UserService, @$rootScope) ->
        @$log.debug "constructing MenuController"

        @home =
            title: 'Home page'
            links:
                content: 'home page'
                url: '/admin/#!/'
        @sinhvien =
            title: 'quan ly sinh vien'
            links:
                content: 'quan ly sinh vien'
                url: '/admin/#!/students'
        @giaovien =
            title: 'quan ly giao vien'
            links:
                content: 'qan ly giao vien'
                url: '/admin/#!/teachers'
        @listluanvan =
            title: 'quan ly luan van'
            links:
                content: 'quan ly luan van'
                url: '/admin/#!/listluanvan'
        @signup =
            title: 'Tao moi user'
            links:
                content: 'quan ly luan van'
                url: '/admin/#!/signup'

controllersModule.controller('MenuCtrl', MenuCtrl)