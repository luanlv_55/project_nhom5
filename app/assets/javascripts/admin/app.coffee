
dependencies = [
    'ngRoute',
    'ngCookies',
    'myApp.filters',
    'myApp.services',
    'myApp.controllers',
    'myApp.directives',
    'myApp.common',
    'myApp.routeConfig',
]

app = angular.module('myApp', dependencies)

angular.module('myApp.routeConfig', ['ngRoute', 'ngCookies'])
.config ($routeProvider, $locationProvider) ->
    $routeProvider
    .when('/', {
                templateUrl: '/assets/partials/home.html'
            })
    .when('/students', {
                templateUrl: '/assets/partials/list.html'
            })
    .when('/students/create', {
                templateUrl: '/assets/partials/detail.html'
            })
    .when('/students/detail/:uuid', {
                templateUrl: '/assets/partials/detail.html'
            })
    .when('/teachers', {
                templateUrl: '/assets/partials/list2.html'
            })
    .when('/teachers/create', {
                templateUrl: '/assets/partials/detail2.html'
            })
    .when('/teachers/detail/:uuid', {
                templateUrl: '/assets/partials/detail2.html'
            })
    .when('/teachers/info/:uuid', {
                templateUrl: '/assets/partials/tdetail.html'
            })
    .when('/listluanvan', {
                templateUrl: '/assets/partials/listlv.html'
            })
    .when('/listluanvan/:id', {
                templateUrl: '/assets/partials/luanvan.html'
            })
    .when('/signup', {
                templateUrl: '/assets/partials/signup.html'
            })
    .otherwise({redirectTo: '/'})

    $locationProvider.html5Mode(true).hashPrefix("!")

.run (@$rootScope, @$location, security, @UserService, @$cookies) ->
    @$rootScope.$on('$routeChangeStart',
        (event, next) ->
            changeMenu = () ->
                console.log "change Menu"
                @$rootScope.menu1 = @$rootScope.menu2 = @$rootScope.menu3 = @$rootScope.menu4 =@$rootScope.menu5 = @$rootScope.menu6 = ''
                if @$rootScope.active = @$location.$$path is '/'
                    @$rootScope.menu1 = 'submenu-active'
                if @$rootScope.active = @$location.$$path.substring(0,7) is '/signin'
                    @$rootScope.menu6 = 'submenu-active'
                if @$rootScope.active = @$location.$$path.substring(0,9) is '/students'
                    @$rootScope.menu2 = 'submenu-active'
                if @$rootScope.active = @$location.$$path.substring(0,9) is '/teachers'
                    @$rootScope.menu3 = 'submenu-active'
                if @$rootScope.active = @$location.$$path.substring(0,12) is '/listluanvan'
                    @$rootScope.menu4 = 'submenu-active'
                if @$rootScope.active = @$location.$$path.substring(0,7) is '/signup'
                    @$rootScope.menu5 = 'submenu-active'

            security.isAuthorized()
            .success(
                () =>
                    changeMenu()
                    @$rootScope.security = {
                        'isAuthorized': true,
                        'email': security.email
                    })
            .error(
                () =>
                    changeMenu()
                    @$rootScope.security = {
                        'isAuthorized': false,
                        'email': ''
                    }



            )


            @role = ''
            if(@$cookies.role isnt undefined)
                @role = @$cookies.role
            if (next.requireAuthorization isnt undefined)
                if (@role not in next.requireAuthorization)
                    if @role is ''
                        $location.path('/signin')
                    else
                        $location.path('/')
    )

@commonModule = angular.module('myApp.common', [])
@controllersModule = angular.module('myApp.controllers', [])
@servicesModule = angular.module('myApp.services', [])
@modelsModule = angular.module('myApp.models', [])
@directivesModule = angular.module('myApp.directives', [])
@filtersModule = angular.module('myApp.filters', [])
