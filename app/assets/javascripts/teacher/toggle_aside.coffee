toggle = () ->
  x = document.getElementById('1col')
  y = document.getElementById('2col')
  aside = document.getElementById('aside')
  if x.disabled == true
    x.disabled = false
    y.disabled = true
    aside.classList.toggle 'noscreen'
    document.getElementById('menu-hide').style.display = 'none'
    document.getElementById('menu-show').style.display = 'block'
  else
    x.disabled = true
    y.disabled = false
    aside.classList.toggle 'noscreen'
    document.getElementById('menu-hide').style.display = 'block'
    document.getElementById('menu-show').style.display = 'none'

jslogout = () ->
  window.location = "http://localhost:9000/login";

root = exports ? this
root.toggle = toggle
root.jslogout = jslogout


