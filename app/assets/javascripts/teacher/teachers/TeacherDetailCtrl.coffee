
class TeacherDetailCtrl

    constructor: (@$log, @$location, @$rootScope, @$routeParams, @TeacherService, @security, @fileUpload, @$route) ->
        @$log.debug "constructing TeacherDetailController"
        @teacher = {}
        @initemail = {}
        @updateOk = false
        @updateError = false
        @updateAvataOk = false
        @updateAvataError = false
        @teacherLogin={}
        @loading = true
        @loading2 = true
        @getTeacher(@$rootScope.email)
        @myFile = null
        @file = {}
        if @teacher._id isnt undefined
            console.log @teacher
        @formOk = false
        @formDisable = ""

    uploadFile: () ->
        console.log("=>>>> uploadFile starting")
        @file = @myFile
        @loading2 = true
        @fileUpload.uploadFileToUrl(@file, @teacher._id.$oid)
        .then(
            (data) =>
                @loading2 = false
                @TeacherService.deleteAvatar(@teacher.avatarID)
                .then(
                    (data) =>
                        console.log "delete old avatar OK!"

                ,
                    (error) =>
                        console.log " delete old avatar error!"
                )
                @teacher.avatarID = data.data
                @TeacherService.updateTeacher(@teacher)
                .then(
                    (data) =>
                        @updateAvataOk = true
                ,
                    (error) =>
                        @updateAvataError = true
                )
        ,
            (error) =>
                @$log.error error
        )

    getTeacher: (email) ->
        @doUpdate = true if email
        if @doUpdate
            @$log.debug "getTeacher(#{email})"

            @TeacherService.getTeacher(email)
            .then(
                (data) =>
                    @$log.debug "Promise returned #{angular.toJson(data, true)} Teacher"
                    @teacher = data
                    @initmssv = @teacher.mssv
                    @initemail = @teacher.email
                    @teacherLogin.email = @teacher.email
                    @loading = false
                    @loading2 = false
            ,
                (error) =>
                    @$log.error "Unable to get Teacher: #{error}"
                    @loading = false
                    @loading2 = false
            )

    updateTeacher: () ->
        @updateError = false
        @loading = true
        @TeacherService.updateTeacher(@teacher)
        .then(
            (data) =>
                @$log.debug "Promise returned #{data} Teacher"
                @updateOk = true
                @loading = false
        ,
            (error) =>
                @$log.error "Unable to update Teacher: #{error}"
                @updateError = true
                @loading = false
        )
        if @teacher.email isnt @teacherLogin.email
            @$log.debug "update teacher ok!"
            @teacherLogin.newEmail =@teacher.email
            @$log.debug @teacherLogin
            @TeacherService.updateEmail(@teacherLogin)
            .then(
                (data) =>
                    @security.signout () ->
                        if($cookies isnt undefined)
                            delete $cookies["email"]
                            delete $cookies["XSRF-TOKEN"]
                            delete $cookies["role"]
                            window.location.href = 'http://localhost:9000/login'
                            #window.location.href = 'http://quanly2.herokuapp.com/login'
                    , () ->
                        console.log "signout Error!"
            ,
                (error) =>
                    @$log.error " error!"
            )

    resetError: () ->
        @teacherCreated = false
        @accountCreated = false
controllersModule.controller('TeacherDetailCtrl', TeacherDetailCtrl)