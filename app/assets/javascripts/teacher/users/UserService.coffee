
class UserService
  @headers = {'Accept': 'application/json', 'Content-Type': 'application/json'}
  @defaultConfig = { headers: @headers }

  constructor: (@$log, @$http, @$q) ->
    @$log.debug "constructing UserService"


  getFiles: (uuid) ->
    @$log.debug "getFiles()"
    deferred = @$q.defer()

    @$http.get("/files/#{uuid}")
    .success((data, status, headers) =>
      @$log.info("Successfully listed Files - status #{status}")
      deferred.resolve(data)
    )
    .error((data, status, headers) =>
      @$log.error("Failed to list Files - status #{status}")
      deferred.reject(data)
    )
    deferred.promise

  uploadFile: (file) ->
    @$log.debug "createUser API"
    deferred = @$q.defer()

    @$http.post("/api/files", file)
    .success((data, status, headers) =>
      @$log.info("Successfully created File - status #{status}")
      deferred.resolve(data)
    )
    .error((data, status, headers) =>
      @$log.error("Failed to create user - status #{status}")
      deferred.reject(data)
    )
    deferred.promise

  getUser: (email) ->
    @$log.debug "getUser()"
    deferred = @$q.defer()

    @$http.get("/user/email/#{email}")
    .success((data, status, headers) =>
      @$log.info("Successfully retrieve User - status #{status}")
      deferred.resolve(data)
    )
    .error((data, status, headers) =>
      @$log.error("Failed to retrieve User - status #{status}")
      deferred.reject(data)
    )
    deferred.promise


  updateUser: (user) ->
    @$log.debug "updateUser #{angular.toJson(user, true)}"
    deferred = @$q.defer()

    @$http.post('/user/update', user)
    .success((data, status, headers) =>
      @$log.info("Successfully update User - status #{status}")
      deferred.resolve(data)
    )
    .error((data, status, headers) =>
      @$log.error("Failed to update user - status #{status}")
      deferred.reject(data)
    )
    deferred.promise

  updateEmail: (userLogin) ->
    deferred = @$q.defer()
    @$http.post('/api/auth/userEmail', userLogin)
    .success((data, status, headers) =>
      deferred.resolve(data)
    )
    .error((data, status, headers) =>
      deferred.reject(data)
    )
    deferred.promise

servicesModule.service('UserService', UserService)