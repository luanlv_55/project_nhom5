class serverAPI
    constructor: (@$log, @$http, @$route) ->
        @$log.debug "constructing serverAPI"

    ensureUnique: (email, success, error) ->
        @$http.get('/api/auth/user/' + email)
        .success((data) ->
            success(data))
        .error(() ->
            error())

    mssvUnique: (mssv, success, error) ->
        @$http.get('/api/user/mssvunique/' + mssv)
        .success((data) ->
            success(data))
        .error(() ->
            error())

    emailUnique: (email, success, error) ->
        @$http.get('/api/user/emailunique/' + email)
        .success((data) ->
            success(data))
        .error(() ->
            error())

    userInfo: (success, error) ->
        @$http.get('/api/auth/user')
        .success((data) ->
            console.log "!! success get User"
            success(data))
        .error( () ->
            console.log "!! Error get User"
            error())

    deleteAccount: (success, error) ->
        @$http.delete('/api/auth/user')
        .success (data) ->
            console.log "!! Success deleteUser"
            success(data)
        .error () ->
            console.log "!! Error deleteUser"
            error()

class security
    constructor: (@$log, @$http, $cookies, @$route, @$location) ->
        @$log.debug "constructing security"



    signup: (userform, success, error) ->
        @$log.debug "SignUp"

        @$http.post('/api/auth/user', userform)
        .success((data, status, headers) =>
            @$log.info("Successfully signed up - status #{status}")
            success()
          #@$route.reload()
        )
        .error((data, status, headers) =>
            @$log.error("Failed to signed up - status #{status}")
            error()
        )


    updatePassword: (userform, success, error) ->
        @$log.debug "update password"

        @$http.post('/api/auth/userPassword', userform)
        .success((data, status, headers) =>
            @$log.info("Successfully signed up - status #{status}")
            success()
                #@$route.reload()
        )
        .error((data, status, headers) =>
            @$log.error("Failed to signed up - status #{status}")
            error()
        )

    signin: (userform, success, error) ->
        $rootScope.email = userform.email
        @$http.post('/api/auth/signin', userform)
        .success((data, status, headers) =>
            @$log.info("Successfully signed in - status #{status}")
            success()
            @$route.reload()
        )
        .error((data, status, headers) =>
            @$log.error("Failed to signed in - status #{status}")
            error()
        )


    signout: (success, error) ->
        @$http.get('/login/')
        .success(() =>
            success()
            @$route.reload()
        )
        .error(() =>
            error()
        )

    isAuthorized: (success, error) ->
        @$log.debug "isAuthorized"
        if $cookies['email'] isnt undefined
            $rootScope.email = $cookies['email'].replace(/\"/g, "")
        @$http.get('/api/auth/authorize')
        .success((data, status, headers) =>
            success
        )
        .error((data, status, headers) =>
            if status is 401
                window.location.href = 'http://quanly2.herokuapp.com/login'
                #window.location = "http://localhost:9000/login"
            error
        )


class fileUpload
    constructor: (@$log, @$http) ->
        @$log.debug "constructing fileUpload service!!"

    uploadFileToUrl: (file, id) ->
        fd = new FormData()
        fd.append 'file', file
        @$http.post("/api/fileupload/#{id}", fd, {
            transformRequest: angular.identity,
            headers: {'Content-Type': undefined}
        })
        .success (data) ->
            console.log("success !!!!#{data}")
        .error (error) ->
            console.log("error !!!!#{error}")

servicesModule.service('security', security)
servicesModule.service('serverAPI', serverAPI)
servicesModule.service('fileUpload', fileUpload)