package models

import play.api.Logger
import play.api.libs.concurrent.Execution.Implicits.defaultContext
import play.api.libs.json.{JsObject, JsValue, Json}
import play.api.mvc._
import play.modules.reactivemongo.{ReactiveMongoPlugin, MongoController}
import play.modules.reactivemongo.json.collection.JSONCollection
import reactivemongo.api.collections.default.BSONCollection
import reactivemongo.api.gridfs.GridFS
import reactivemongo.api.indexes.{IndexType, Index}
import services.EmailService._



object Models extends Controller with MongoController  {
    private final val logger = Logger
    val gridFS = new GridFS(db)

    // let's build an index on our gridfs chunks collection if none
    gridFS.ensureIndex().onComplete {
        case index =>
            Logger.info(s"Checked index, result is $index")
    }

    /*
     * Get a JSONCollection (a Collection implementation that is designed to work
     * with JsObject, Reads and Writes.)
     * Note that the `collection` is not a `val`, but a `def`. We do _not_ store
     * the collection reference to avoid potential problems in development with
     * Play hot-reloading.
     */

    def collectionUsers = {
        val collectionUsers: JSONCollection = db.collection[JSONCollection]("users")
        collectionUsers.indexesManager.ensure(
            Index(List("mssv" -> IndexType.Ascending, "email" -> IndexType.Ascending), unique = true)
        )
        collectionUsers
    }

    def collectionTeachers = {
        val collectionTeachers: JSONCollection = db.collection[JSONCollection]("teachers")
        collectionTeachers.indexesManager.ensure(
            Index(List("email" -> IndexType.Ascending), unique = true)
        )
        collectionTeachers
    }

    def collectionUser = {
        val collectionUser: JSONCollection = db.collection[JSONCollection]("user")
        collectionUser.indexesManager.ensure(
            Index(List("email" -> IndexType.Ascending), unique = true)
        )
        collectionUser
    }


    def collectionFiles: JSONCollection = db.collection[JSONCollection]("fs.files")

    def collectionToken = {
        val collectionToken:JSONCollection = db.collection[JSONCollection]("tokens")
        collectionToken.indexesManager.ensure(
            Index(List("ma" -> IndexType.Ascending, "email" -> IndexType.Ascending), unique = true)
        )
        collectionToken
    }
    def collectionLuanvan = {
        val collectionLuanvan:JSONCollection = db.collection[JSONCollection]("luanvan")
        collectionLuanvan.indexesManager.ensure(
            Index(List("ma" -> IndexType.Ascending, "email" -> IndexType.Ascending), unique = true)
        )
        collectionLuanvan
    }

    def findUserByMssv(json: JsValue) =
        collectionUsers
            .find(Json.obj("mssv" -> json \ "mssv"))
            .cursor[JsObject].collect[List]().map(_.headOption)

    def findUserByEmail(json: JsValue) =
        collectionUsers
            .find(Json.obj("email" -> json \ "email"))
            .cursor[JsObject].collect[List]().map(_.headOption)

    def findUserAccountByEmail(json: JsValue) =
        collectionUser
            .find(Json.obj("email" -> json \ "email"))
            .cursor[JsObject].collect[List]().map(_.headOption)

    def findToken(tokenx: String) =
        collectionToken
            .find(Json.obj("token" ->  tokenx))
            .cursor[JsObject].collect[List]().map(_.headOption)

}
