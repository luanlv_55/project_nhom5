package controllers

import javax.inject.{Inject, Singleton}

import actors.{UUIDActor, UserActor}
import akka.actor.{ActorRef, Props}
import akka.pattern.ask
import akka.util.Timeout
import controllers.Auth._
import models.{UserValid, Tokens}
import play.api.Logger
import play.api.libs.concurrent.Execution.Implicits._
import play.api.libs.iteratee.{Concurrent, Iteratee}
import play.api.libs.json.JsValue
import play.api.mvc._
//import play.filters.csrf.{CSRFAddToken, CSRFCheck}
import play.libs.{ Akka}
import play.api.Logger
import play.api.libs.concurrent.Execution.Implicits.defaultContext
import play.api.libs.iteratee.{Enumerator, Iteratee}
import play.api.libs.json.{JsValue, Json, _}
import play.api.mvc._
import play.modules.reactivemongo.MongoController
import play.modules.reactivemongo.json.collection.JSONCollection
import reactivemongo.api.gridfs.{DefaultFileToSave, GridFS}
import reactivemongo.api.{Cursor, QueryOpts}
import reactivemongo.bson._
import reactivemongo.core.commands._
import services.UUIDGenerator
import scala.concurrent.Future
import scala.concurrent.duration._
import play.modules.reactivemongo.json.BSONFormats._
import play.modules.reactivemongo.json.BSONFormats

import models.Models._
/**
 * Instead of declaring an object of Application as per the template project, we must declare a class given that
 * the application context is going to be responsible for creating it and wiring it up with the UUID generator service.
 * @param uuidGenerator the UUID generator service we wish to receive.
 */
@Singleton
class Application   @Inject() (uuidGenerator: UUIDGenerator) extends Controller with MongoController{

  implicit val app: play.api.Application = play.api.Play.current

  private final val logger = Logger

  lazy val CacheExpiration =
    app.configuration.getInt("cache.expiration").getOrElse(60 /*seconds*/ * 2 /* minutes */)

  implicit val timeout = Timeout(5 seconds)

  lazy val uuidActor : ActorRef = Akka.system.actorOf(Props(new UUIDActor(uuidGenerator)))

  def detail = Action {
    implicit request => Ok(views.html.templates.detail())
  }

  def index = GetAction {
    Request =>
      Redirect(routes.Application.login())
  }
// .map(d => Json.toJson(d))
  def login = GetAction {
  request =>
        Ok(views.html.login())
  }

//  def admin = GetAction {
//    request =>
//        Ok(views.html.admin())
//  }
  def admin = GetAction {
    request => {
      if ((request.session.get("email").getOrElse("").length() > 0) && (request.session.get("role").getOrElse("") == "admin")) {
        Ok(views.html.admin())
      }else{
        Redirect(routes.Application.login())
      }
    }
  }
  def student = GetAction {
    request => {
      if ((request.session.get("email").getOrElse("").length() > 0) && (request.session.get("role").getOrElse("") == "student")) {
        Ok(views.html.student())
      }else{
        Redirect(routes.Application.login())
      }
    }
  }

  def teacher = GetAction {
    request => {
      if ((request.session.get("email").getOrElse("").length() > 0) && (request.session.get("role").getOrElse("") == "teacher")) {
        Ok(views.html.teacher())
      }else{
        Redirect(routes.Application.login())
      }
    }
  }


  def test = Action.async {
    request => {
      val command = Aggregate(collectionUsers.name, Seq(
        Match(BSONDocument("mssv" -> BSONDocument("$eq" -> "1"))),
        Match(BSONDocument("name" -> BSONDocument("$eq" -> "5")))
      ))

      // we get a Future[BSONDocument]
      val result = db.command(command).map(d => Json.toJson(d))
      result.map { value =>
          if(value.toString() == "[]")
            Ok("Khong thay")
          else
            Ok(value.toString())
      }
    }
  }

  def getNameAttachment(id: String) = Action.async {
    request => {
      val command = Aggregate(collectionFiles.name, Seq(
        Match(BSONDocument("metadata.group" -> BSONDocument("$eq" -> id)))
      ))

      // we get a Future[BSONDocument]
      val result = db.command(command).map(d => Json.toJson(d))
      result.map { value =>
        if(value.toString() == "[]")
          Ok("Error")
        else
          Ok(value)
      }
    }
  }

  def getLuanVan(uuid: String) = Action.async {
    request => {
      val command = Aggregate(collectionLuanvan.name, Seq(
        Match(BSONDocument("giaovienID" -> BSONDocument("$eq" -> uuid))),
        Match(BSONDocument("chapnhan" -> BSONDocument("$eq" -> true)))
      ))

      // we get a Future[BSONDocument]
      val result = db.command(command).map(d => Json.toJson(d))
      result.map { value =>
        if(value.toString() == "[]")
          Ok("")
        else
          Ok(value)
      }
    }
  }


  def getPassword = GetAction {
    request =>
      Ok(views.html.resetpassword())
  }

  //load web dat mat khau
  def setPassword(tokenx: String) = Action.async { request =>
    val cursor: Cursor[Tokens] = collectionToken.
        find(Json.obj("token" -> tokenx)).
        cursor[Tokens]
    val futureTokensList: Future[List[Tokens]] = cursor.collect[List]()
    val futureTokensJsonArray: Future[Tokens] = futureTokensList.map { tokens =>
      tokens.head
    }
    futureTokensJsonArray.map {
      token =>
        Ok(views.html.setpassword(token.email, token.action, tokenx))
    }.recover {
      case e =>
        e.printStackTrace()
        BadRequest(e.getMessage())
    }
  }

  //load web dat lai mat khau moi
  def setNewPassword(tokenx: String) = Action.async { request =>
    val cursor: Cursor[Tokens] = collectionToken.
        find(Json.obj("token" -> tokenx)).
        cursor[Tokens]
    val futureTokensList: Future[List[Tokens]] = cursor.collect[List]()
    val futureTokensJsonArray: Future[Tokens] = futureTokensList.map { tokens =>
      tokens.head
    }
    futureTokensJsonArray.map {
      token =>
        Ok(views.html.setnewpassword(token.email, tokenx))
    }.recover {
      case e =>
        e.printStackTrace()
        BadRequest(e.getMessage())
    }
  }


  // Api tao token reset password
  def tokenResetPassword = Action.async(parse.json) {
    request =>
      collectionToken
          .find(Json.obj("email" -> request.body \ "email"))
          .cursor[JsObject].collect[List]().map(_.headOption) .map {
        _ match {
          case None =>
              BadRequest("Email not exist")
          case Some(tokenx) => {
              val newToken = java.util.UUID.randomUUID().toString()
              collectionToken.update(Json.obj("email" -> request.body \ "email"), Json.obj("$set" -> Json.obj("action" -> "reset")))
              collectionToken.update(Json.obj("email" -> request.body \ "email"), Json.obj("$set" -> Json.obj("token" -> newToken)))
              import services.EmailService._
              sendEmailWithTokenReset((request.body \ "email").toString.slice(1, (request.body \ "email").toString.length - 1), "Đặt lại mật khẩu", newToken)
          }
          Ok
        }
      }
  }

  //API dat mat khau lan dau
  def setFirstPassword = Action.async(parse.json) {
    request =>
      findToken((request.body \ "token").toString.slice(1, (request.body \ "token").toString.length - 1)) map {
        _ match {
          case None =>
            BadRequest("Khong ton tai")
          case Some(tokenx) => {
            findUserAccountByEmail(Json.obj("email" ->
                (request.body \ "email").toString.slice(1, (request.body \ "email").toString.length - 1))) map {
              _ match {
                case None =>
                  BadRequest("Email not exist!")
                case Some(user) => {
                  val updateUser = user ++ Json.obj("password" ->
                      Json.toJson((request.body \ "password").toString.slice(1, (request.body \ "password").toString.length -1)))
                  collectionUser.update(Json.obj("email" ->
                      (request.body \ "email").toString.slice(1, (request.body \ "email").toString.length - 1)), updateUser)
                }
              }
            }
            collectionToken.update(Json.obj("email" -> request.body \ "email"), Json.obj("$set" -> Json.obj("action" -> "done")))
            collectionToken.update(Json.obj("email" -> request.body \ "email"), Json.obj("$set" -> Json.obj("token" -> JsNull)))
            collectionUsers.update(Json.obj("email" -> request.body \ "email"), Json.obj("$set" -> Json.obj("active" -> true)))
            collectionTeachers.update(Json.obj("email" -> request.body \ "email"), Json.obj("$set" -> Json.obj("active" -> true)))
            Ok
          }
        }
      }
  }


  //dat lai mat khau
  def APIsetPassword = Action.async(parse.json) {
    request =>
      findToken((request.body \ "token").toString.slice(1, (request.body \ "token").toString.length - 1)) map {
        _ match {
          case None =>
            BadRequest("Email not exist")
          case Some(tokenx) => {
            collectionToken.update(Json.obj("email" -> request.body \ "email"), Json.obj("$set" -> Json.obj("action" -> "done")))
            collectionToken.update(Json.obj("email" -> request.body \ "email"), Json.obj("$set" -> Json.obj("token" -> JsNull)))
            collectionUser.update(Json.obj("email" -> request.body \ "email"), Json.obj("$set" -> Json.obj("password" -> (request.body \ "password").toString.slice(1, (request.body \ "password").toString.length -1))))
            Ok
          }
        }
      }
  }





  def randomUUID = Action.async {
    logger.info("calling UUIDGenerator...")
    (uuidActor ? Some) map { case x: String => Ok(x) }
  }

  def ws = WebSocket.using[JsValue] { request =>
    logger.info("User Connected")

    val (out, channel) = Concurrent.broadcast[JsValue]

    val userActor = Akka.system.actorOf(Props(classOf[UserActor], channel))

    val in = Iteratee.foreach[JsValue] { message =>
      logger.info(s"Received message: $message")
      userActor ! message
    }.map { _ =>
      logger.info("User Disconnected")
      Akka.system.stop(userActor)
    }
    (in, out)
  }



  object PostAction extends ActionBuilder[Request] {
    def invokeBlock[A](request: Request[A], block: (Request[A]) => Future[Result]) = {
      // authentication code here
      block(request)
    }
    //override def composeAction[A](action: Action[A]) = CSRFCheck(action)
  }

  object GetAction extends ActionBuilder[Request] {
    def invokeBlock[A](request: Request[A], block: (Request[A]) => Future[Result]) = {
      //val userSession = CSRF.getToken(request).map(_.value).getOrElse("")
      //logger.info(s"GetAction $userSession")
      // authentication code here
      block(request)
    }
    //override def composeAction[A](action: Action[A]) = CSRFAddToken(action)
  }

}
