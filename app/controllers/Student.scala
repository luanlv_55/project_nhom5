package controllers

import javax.inject.{Inject, Singleton}

import com.sksamuel.scrimage.{Format, Image}
import models.Models._
import models.UserValid
import org.joda.time.DateTime
import play.api.Logger
import play.api.libs.concurrent.Execution.Implicits.defaultContext
import play.api.libs.iteratee.{Enumerator, Iteratee}
import play.api.libs.json.{JsValue, Json, _}
import play.api.mvc._
import play.modules.reactivemongo.MongoController
import play.modules.reactivemongo.json.collection.JSONCollection
import reactivemongo.api.gridfs.{DefaultFileToSave, GridFS}
import reactivemongo.api.{Cursor, QueryOpts}
import reactivemongo.bson._
import reactivemongo.core.commands.Count
import services.UUIDGenerator
import reactivemongo.api.gridfs.Implicits._


import models.Formats._

import scala.concurrent.Future

@Singleton
class Student @Inject()(uuidGenerator: UUIDGenerator) extends Controller with MongoController with Secured {
    private final val logger = Logger


    def listTeacher(page: Int, perPage: Int) = Action.async {
        // let's do our query
        val cursor: Cursor[UserValid] = collectionUsers.
            // find all
            find(Json.obj()).
            // pagination
            options(QueryOpts(page * perPage)).
            // sort them by creation date
            sort(Json.obj("created" -> -1)).
            // perform the query and get a cursor of JsObject
            cursor[UserValid]
        // gather all the JsObjects in a list
        val futureUsersList: Future[Seq[UserValid]] = cursor.collect[Seq](perPage)
        logger.info(futureUsersList.toString)
        // transform the list into a JsArray
        val futurePersonsJsonArray: Future[JsArray] = futureUsersList.map { users =>
            Json.arr(users)
        }
        // everything's ok! Let's reply with the array
        futurePersonsJsonArray.map {
            users =>
                Ok(users(0))
        }
    }

}
